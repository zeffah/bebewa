package com.zeffah.shoppa.modelService;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shoppa.model.OrderItem;
import com.zeffah.shoppa.model.OrderItem_Table;
import com.zeffah.shoppa.model.Product;

import java.util.List;

public class OrderService {
    public static List<OrderItem> orderItems() {
        return SQLite.select().from(OrderItem.class).queryList();
    }

    public static OrderItem orderItem(int productId) {
        return SQLite.select().from(OrderItem.class).where(OrderItem_Table.product_id.eq(productId)).querySingle();
    }

    public static double orderAmount() {
        double amount = 0;
        List<OrderItem> orderItems = orderItems();
        for (OrderItem orderItem : orderItems) {
            orderItem.product.load();
            double orderItemAmount = orderItem.product.getProductPrice() * orderItem.getItemQuantity();
            amount += orderItemAmount;
        }
        return amount;
    }

    public static boolean checkIfProductExistCart(List<Product> mList, String barCode) {
        for (int i = 0; i < mList.size(); i++) {
            Product product = mList.get(i);
            String apiBarCode = product.getBarCode();
            if (apiBarCode.equals(barCode)) {
                return true;
            }
        }
        return false;
    }

    public static OrderItem updateCartItem(List<Product> sourceList, OrderItem cartItem) {
        try {
            for (Product product : sourceList) {
                if (product.getBarCode().equals(cartItem.getBarCode())) {
                    cartItem.product = product;
                    cartItem.product.setProductPrice(product.getProductPrice());
                    cartItem.product.save();
                    cartItem.setBarCode(product.getBarCode());
                    cartItem.setItemName(product.getProductName());
                    cartItem.save();
                }
            }
            return cartItem;
        } catch (Exception e) {
            return cartItem;
        }
    }

    public static boolean addItemToCart(Product product) {
        OrderItem orderItem = OrderService.orderItem(product.getProductId());
        if (orderItem != null) {
            orderItem.setItemQuantity(orderItem.getItemQuantity() + 1);
            return orderItem.save();
        } else {
            orderItem = new OrderItem();
            orderItem.setItemName(product.getProductName());
            orderItem.setBarCode(product.getBarCode());
            orderItem.setItemQuantity(1);
            orderItem.setItemType(product.getProductDetails());
            orderItem.product = product;
            orderItem.product.save();
            return orderItem.save();
        }
    }

    public static double updateCurrentCart(List<OrderItem> cartItems, List<Product> productList){
        if (!cartItems.isEmpty()) {
            double sum = 0;
            for (OrderItem cartItem : cartItems) {
                String barCode = cartItem.getBarCode();
                boolean existsInProductList = OrderService.checkIfProductExistCart(productList, barCode);
                if (!existsInProductList) {
                    cartItem.delete();
                } else {
                    OrderItem orderItem = OrderService.updateCartItem(productList, cartItem);
                    double itemTotal = cartItem.getItemQuantity() * orderItem.product.getProductPrice();
                    sum += itemTotal;
                }
            }
            return sum;
        }
        return 0f;
    }
}
