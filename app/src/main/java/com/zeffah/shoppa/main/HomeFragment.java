package com.zeffah.shoppa.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.zeffah.shoppa.R;
import com.zeffah.shoppa.callback.OnFragmentInteractionListener;
import com.zeffah.shoppa.factory.AppViewModel;
import com.zeffah.shoppa.factory.GlobalMethods;
import com.zeffah.shoppa.model.OrderItem;
import com.zeffah.shoppa.model.Product;
import com.zeffah.shoppa.model.Store;
import com.zeffah.shoppa.modelService.OrderService;

import java.util.List;

public class HomeFragment extends Fragment {
    private AppViewModel model;
    private OnFragmentInteractionListener mListener;
    private MaterialSpinner spnStoreList, spnStoreBranch;
    private Context context;
    private Button btnSelectStore;
    private Store selectedStore = null;
    private View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnSelectStore) {
                if (selectedStore != null) {
                    FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.main_content, ProductFragment.newInstance(selectedStore.getStoreName(), 1)).addToBackStack("ProductFragment").commit();
                    return;
                }
                Snackbar.make(v, "Please select preferred store to continue", Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
        model = ViewModelProviders.of(((AppCompatActivity)context)).get(AppViewModel.class);
        if (actionBar != null) {
            actionBar.hide();
        }
        if (getArguments() != null) {
        }
        GlobalMethods.generateStores();
        GlobalMethods.generateProductList();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        spnStoreList = view.findViewById(R.id.spn_select_shop);
        spnStoreBranch = view.findViewById(R.id.spn_select_store_branch);
        btnSelectStore = view.findViewById(R.id.btn_select_store);
        btnSelectStore.setOnClickListener(onClick);

        return view;
    }

    @Override @SuppressWarnings("unchecked")
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GlobalMethods.fetchStoreList(new GlobalMethods.OnFetchListener() {
            @Override
            public void onFetchSuccess(List<?> results) {
                model.setStores((List<Store>) results);
                spnStoreList.setItems(results);
            }
        });
        spnStoreList.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectedStore = (Store) item;
                model.setStoreProducts(selectedStore);

                List<Product> productList = model.getStoreProducts().getValue();
                List<OrderItem> orderItems = OrderService.orderItems();

                if (productList != null && !orderItems.isEmpty())OrderService.updateCurrentCart(orderItems, productList);
            }
        });
    }

    public void onButtonPressed(String uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            this.mListener = (OnFragmentInteractionListener) context;
            this.context = context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
        this.context = null;
    }
}
