package com.zeffah.shoppa.main;

import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.raizlabs.android.dbflow.sql.language.Method;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shoppa.R;
import com.zeffah.shoppa.factory.AppViewModel;
import com.zeffah.shoppa.factory.GlobalMethods;
import com.zeffah.shoppa.listadapter.ProductRecyclerViewAdapter;
import com.zeffah.shoppa.model.OrderItem;
import com.zeffah.shoppa.model.OrderItem_Table;
import com.zeffah.shoppa.model.Product;
import com.zeffah.shoppa.model.Store;
import com.zeffah.shoppa.modelService.OrderService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProductFragment extends Fragment implements ProductRecyclerViewAdapter.OnAddItemToCart, SearchView.OnQueryTextListener {
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_SHOP_NAME = "shop_name";
    private List<Product> productList;
    private AppViewModel model;
    private Context context;
    private int mColumnCount = 1;
    private int itemCount = 0;
    private double orderAmount = 0;
    private String mShopName;
    private OnListFragmentInteractionListener mListener;
    private MaterialSpinner spnSelectStore;
    private TextView txtShopName, txtOderAmount, lblOrderAmount;
    private AppCompatTextView txtBadge;
    private AppCompatButton btnGoToCart;
    private ActionBar actionBar;
    private ProductRecyclerViewAdapter productRecyclerViewAdapter;
    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnGoToCart) {
                FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.main_content, CartFragment.newInstance(mShopName, orderAmount)).addToBackStack("CartFragment").commit();
            }
        }
    };

    public ProductFragment() {
    }

    @SuppressWarnings("unused")
    public static ProductFragment newInstance(String shopName, int columnCount) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_SHOP_NAME, shopName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        productList = new ArrayList<>();
        actionBar = ((AppCompatActivity) context).getSupportActionBar();
        model = ViewModelProviders.of((AppCompatActivity) context).get(AppViewModel.class);
        if (actionBar != null) {
            GlobalMethods.setActionBarTitle(actionBar, String.valueOf(mShopName + " Supermarket"), true);
        }
        if (getArguments() != null) {
            Bundle args = getArguments();
            mColumnCount = args.getInt(ARG_COLUMN_COUNT);
            mShopName = args.getString(ARG_SHOP_NAME);
        }
        itemCount = (int) SQLite.select(Method.sum(OrderItem_Table.quantity)).from(OrderItem.class).count();
        orderAmount = OrderService.orderAmount();
    }

    @Override
    public void onResume() {
        super.onResume();
        actionBar = ((AppCompatActivity) context).getSupportActionBar();
        itemCount = (int) SQLite.select(Method.sum(OrderItem_Table.quantity)).from(OrderItem.class).count();
        orderAmount = OrderService.orderAmount();
        setQtyAndAmount(itemCount, orderAmount);
        if (actionBar != null) {
            GlobalMethods.setActionBarTitle(actionBar, String.valueOf(mShopName + " Supermarket"), true);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);
        Context context = view.getContext();
        final RecyclerView recyclerView = view.findViewById(R.id.list);
        txtShopName = view.findViewById(R.id.lbl_shop_name);
        txtOderAmount = view.findViewById(R.id.txt_total_amount);
        lblOrderAmount = view.findViewById(R.id.lbl_total_amount);
        txtBadge = view.findViewById(R.id.badge);
        spnSelectStore = view.findViewById(R.id.spn_select_shop);
        btnGoToCart = view.findViewById(R.id.btn_go_to_cart);
        btnGoToCart.setOnClickListener(viewClickListener);
        model.getStores().observe(this, new Observer<List<Store>>() {
            @Override
            public void onChanged(@Nullable List<Store> stores) {
                if (stores != null) {
                    spnSelectStore.setItems(stores);
                }
            }
        });
        setQtyAndAmount(itemCount, orderAmount);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        txtShopName.setText(String.format(Locale.getDefault(), "%s Supermarket", mShopName));
        model.getStoreProducts().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(@Nullable List<Product> products) {
                productList = products;
                productRecyclerViewAdapter = new ProductRecyclerViewAdapter(productList, ProductFragment.this);
                recyclerView.setAdapter(productRecyclerViewAdapter);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spnSelectStore.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                Store store = (Store) item;
                mShopName = store.getStoreName();
                updateShopNameText(mShopName);
                model.setStoreProducts(store);
                productList = GlobalMethods.getStoreProducts(store);
                productRecyclerViewAdapter.updateList(productList);

                //check if there is cart Item
                List<OrderItem> cartItems = OrderService.orderItems();
                orderAmount = OrderService.updateCurrentCart(cartItems, productList);
                if (orderAmount > 0f){
                    setQtyAndAmount(itemCount, orderAmount);
                }
            }
        });
    }

    private void updateShopNameText(String shopName) {
        txtShopName.setText(String.format(Locale.getDefault(), "%s Supermarket", shopName));
        GlobalMethods.setActionBarTitle(actionBar, String.valueOf(shopName + " Supermarket"), true);
    }

    private void setQtyAndAmount(int itemCount, double orderAmount) {
        if (itemCount > 0) {
            txtBadge.setVisibility(View.VISIBLE);
            txtOderAmount.setVisibility(View.VISIBLE);
            lblOrderAmount.setVisibility(View.VISIBLE);
            txtBadge.setText(String.valueOf(itemCount));
            txtOderAmount.setText(String.valueOf("Ksh." + orderAmount));
        } else {
            txtBadge.setVisibility(View.GONE);
            txtOderAmount.setVisibility(View.GONE);
            lblOrderAmount.setVisibility(View.GONE);
            txtBadge.setText(String.valueOf(itemCount));
            txtOderAmount.setText(String.valueOf("Ksh." + orderAmount));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        context = null;
    }

    @Override
    public void addToCart(Product product) {
        itemCount += 1;
        boolean isAdded = OrderService.addItemToCart(product);
        orderAmount += product.getProductPrice();
        if (isAdded) {
            setQtyAndAmount(itemCount, orderAmount);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product_list_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search_product);
        SearchManager searchManager = (SearchManager) context.getSystemService(Context.SEARCH_SERVICE);
        SearchView mSearchView = (SearchView) menuItem.getActionView();
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        mSearchView.setLayoutParams(params);
        mSearchView.setOnQueryTextListener(this);
        if (searchManager != null)
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(((AppCompatActivity) context).getComponentName()));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        productRecyclerViewAdapter.getFilter().filter(newText);
        return true;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Product item);
    }
}
