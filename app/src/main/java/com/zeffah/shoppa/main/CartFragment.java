package com.zeffah.shoppa.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.zeffah.shoppa.R;
import com.zeffah.shoppa.callback.OnCartBottomSheetDismiss;
import com.zeffah.shoppa.factory.GlobalMethods;
import com.zeffah.shoppa.listadapter.CartRecyclerViewAdapter;
import com.zeffah.shoppa.model.BestStore;
import com.zeffah.shoppa.model.OrderItem;
import com.zeffah.shoppa.modelService.OrderService;

import java.util.List;

public class CartFragment extends Fragment implements CartRecyclerViewAdapter.OnQuantityChange {
    public static final String ACTION_REDUCE_CART = "reduce";
    public static final String ACTION_INCREASE_CART = "increase";
    public static final String ARG_SHOPPING_AMOUNT = "shopping-amount";
    private static final String ARG_SHOP_NAME = "shop_name";
    private Context context;
    private CartRecyclerViewAdapter cartRecyclerViewAdapter;
    private List<OrderItem> orderItems;
    private ActionBar actionBar;
    private ImageButton btnProceedToCheckout;
    private TextView txtShoppingAmount, txtStoreName;

    private String storeName;
    private double shoppingAmount;
    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnProceedToCheckout) {
                GlobalMethods.cartDialog(v.getContext(), new OnCartBottomSheetDismiss() {
                    @Override
                    public void onDismiss(BestStore best) {
                        FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.main_content, CardFragment.newInstance()).addToBackStack("CheckoutFragment").commit();
                        return;
                    }
                }).show();
            }
        }
    };

    public CartFragment() {
    }

    public static CartFragment newInstance(String storeName, double shoppingAmount) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SHOP_NAME, storeName);
        args.putDouble(ARG_SHOPPING_AMOUNT, shoppingAmount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = ((AppCompatActivity) context).getSupportActionBar();
        GlobalMethods.setActionBarTitle(actionBar, String.valueOf("Shopping Cart"),true);
        if (getArguments() != null) {
            storeName = getArguments().getString(ARG_SHOP_NAME);
            shoppingAmount = getArguments().getDouble(ARG_SHOPPING_AMOUNT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        GlobalMethods.setActionBarTitle(actionBar, String.valueOf("Shopping Cart"), true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        orderItems = OrderService.orderItems();
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        txtShoppingAmount = view.findViewById(R.id.txt_shopping_amount);
        txtStoreName = view.findViewById(R.id.txt_selected_store);
        btnProceedToCheckout = view.findViewById(R.id.btn_proceed_to_checkout);
        btnProceedToCheckout.setOnClickListener(viewClickListener);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        cartRecyclerViewAdapter = new CartRecyclerViewAdapter(orderItems, this);
        recyclerView.setAdapter(cartRecyclerViewAdapter);

        setStoreAndAmount(storeName, shoppingAmount);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    private void setStoreAndAmount(String storeName, double shoppingAmount) {
        txtStoreName.setText(storeName);
        txtShoppingAmount.setText(String.valueOf("Ksh." + shoppingAmount));
    }

    @Override
    public void onChange(String action, OrderItem orderItem, int newQty) {
        double itemPrice = orderItem.product.getProductPrice();
        switch (action) {
            case ACTION_INCREASE_CART:
                shoppingAmount += itemPrice;
                break;
            case ACTION_REDUCE_CART:
                shoppingAmount -= itemPrice;
                break;
            default:
                double orderItemAmount = orderItem.getItemQuantity() * itemPrice;
                double newItemAmount =  newQty * itemPrice;
                shoppingAmount = (shoppingAmount - orderItemAmount)+newItemAmount;
                break;
        }
        txtShoppingAmount.setText(String.valueOf("Ksh." + shoppingAmount));
    }
}
