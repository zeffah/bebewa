package com.zeffah.shoppa.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stripe.android.model.Card;
import com.stripe.android.view.CardInputWidget;
import com.stripe.android.view.CardMultilineWidget;
import com.zeffah.shoppa.R;

public class CardFragment extends Fragment {
    private Button btnPayNow;
    private CardMultilineWidget inputWidget;

    public CardFragment() {

    }

    public static CardFragment newInstance() {
        CardFragment fragment = new CardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);
        inputWidget = view.findViewById(R.id.card_input_widget);
        btnPayNow = view.findViewById(R.id.btn_pay_now);
        btnPayNow.setOnClickListener(viewClickListener);
        return view;
    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnPayNow){
                if (isCardValid()){
                    Snackbar.make(v, "Card is valid", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                Snackbar.make(v, "Card is invalid", Snackbar.LENGTH_SHORT).show();
            }
        }
    };

    private boolean isCardValid(){
        Card card = inputWidget.getCard();
        return card != null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
