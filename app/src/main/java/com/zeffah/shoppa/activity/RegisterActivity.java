package com.zeffah.shoppa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.zeffah.shoppa.R;
import com.zeffah.shoppa.dialog.CustomDialog;
import com.zeffah.shoppa.factory.FormValidation;
import com.zeffah.shoppa.factory.GlobalMethods;
import com.zeffah.shoppa.model.User;

import java.util.List;

public class RegisterActivity extends AppCompatActivity {
    private EditText edtFirstName, edtLastName, edtPhoneNumber, edtEmailAddress, edtUserName, edtPassword;
    private Button btnRegister;
    private View view;
    private View.OnClickListener onViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnRegister) {
                List<String> errorsArray = FormValidation.registrationFormValid(view);
                if (errorsArray == null || errorsArray.isEmpty()) {
                    if (registerUser()) {
                        CustomDialog.successAlert(RegisterActivity.this, "Registration Successful", CFAlertDialog.CFAlertStyle.NOTIFICATION).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }, 1500);
                    }
                } else {
                    String[] array = new String[errorsArray.size()];
                    array = errorsArray.toArray(array);
                    CustomDialog.formSubmitError(v.getContext(), "User Registration", array, CFAlertDialog.CFAlertStyle.NOTIFICATION).show();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        view = findViewById(R.id.registration_layout);
        edtFirstName = findViewById(R.id.edt_first_name);
        edtLastName = findViewById(R.id.edt_last_name);
        edtPhoneNumber = findViewById(R.id.edt_phone_number);
        edtEmailAddress = findViewById(R.id.edt_email_address);
        edtUserName = findViewById(R.id.edt_username);
        edtPassword = findViewById(R.id.edt_password);

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(onViewClick);
    }

    private boolean registerUser() {
        String username = edtUserName.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        String firstName = edtFirstName.getText().toString().trim();
        String lastName = edtLastName.getText().toString().trim();
        String email = edtEmailAddress.getText().toString().trim();
        String phone = edtPhoneNumber.getText().toString().trim();
        String customerId = GlobalMethods.generateRandomString(5);
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmailAddress(email);
        user.setPhoneNumber(phone);
        user.setCustomerId(customerId);

        return user.save();
    }
}
