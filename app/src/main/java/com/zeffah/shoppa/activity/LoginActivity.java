package com.zeffah.shoppa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.raizlabs.android.dbflow.StringUtils;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;
import com.zeffah.shoppa.R;
import com.zeffah.shoppa.dialog.CustomDialog;
import com.zeffah.shoppa.model.User;
import com.zeffah.shoppa.model.User_Table;

public class LoginActivity extends AppCompatActivity {
    private Button btnSignIn, btnSignUp;
    private EditText edtUsername, edtPassword;
    private View.OnClickListener onViewClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == btnSignIn) {
                String username = edtUsername.getText().toString().trim();
                String password = edtPassword.getText().toString().trim();
                if (StringUtils.isNullOrEmpty(username) || StringUtils.isNullOrEmpty(password)) {
                    Snackbar.make(v, "Provide username and password", Snackbar.LENGTH_SHORT).show();
                } else {
                    loginUser(username, password, v);
                }
                return;
            }

            if (v == btnSignUp) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsername = findViewById(R.id.edt_username);
        edtPassword = findViewById(R.id.edt_password);

        btnSignIn = findViewById(R.id.btn_login);
        btnSignUp = findViewById(R.id.btn_sign_up);

        btnSignIn.setOnClickListener(onViewClick);
        btnSignUp.setOnClickListener(onViewClick);

    }

    private void loginUser(String username, String password, final View anchor) {
        SQLite.select().from(User.class).where(User_Table.username.eq(username)).and(User_Table.password.eq(password))
                .async()
                .querySingleResultCallback(new QueryTransaction.QueryResultSingleCallback<User>() {
                    @Override
                    public void onSingleQueryResult(QueryTransaction transaction, @Nullable User user) {
                        if (user != null) {
                        CustomDialog.successAlert(LoginActivity.this, "Login Successful", CFAlertDialog.CFAlertStyle.NOTIFICATION).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            }
                        }, 1500);
                        } else {
                            Snackbar.make(anchor, "Wrong username or password", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }).error(new Transaction.Error() {
                    @Override
                    public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {
                        transaction.cancel();
                    }
        }).execute();
    }
}
