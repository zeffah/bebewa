package com.zeffah.shoppa.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.zeffah.shoppa.R;
import com.zeffah.shoppa.callback.OnFragmentInteractionListener;
import com.zeffah.shoppa.main.HomeFragment;
import com.zeffah.shoppa.main.ProductFragment;
import com.zeffah.shoppa.model.Product;
import com.zeffah.shoppa.model.ProductList;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener, ProductFragment.OnListFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.main_content, new HomeFragment()).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String uri) {
        Toast.makeText(this, ""+uri, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onListFragmentInteraction(Product item) { }
}
