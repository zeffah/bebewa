package com.zeffah.shoppa.dialog;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.zeffah.shoppa.R;
import com.zeffah.shoppa.callback.OnAddToCartListener;
import com.zeffah.shoppa.model.OrderItem;
import com.zeffah.shoppa.model.Product;

public class CustomDialog {

    public static CFAlertDialog addItemToOrderList(final Context context, Product product, CFAlertDialog.CFAlertStyle cfAlertStyle, OnAddToCartListener addToCartListener) {
        View view = LayoutInflater.from(context).inflate(R.layout.order_item_layout, null, false);
        final EditText edtQuantity = view.findViewById(R.id.edt_order_quantity);
        Button btnAddToSale = view.findViewById(R.id.btn_add_to_sale_list);
        CFAlertDialog.Builder dialogBuilder = new CFAlertDialog.Builder(context)
                .setFooterView(view)
                .setTitle(product.getProductName())
                .setDialogStyle(cfAlertStyle)
                .setTextGravity(Gravity.START);

        final CFAlertDialog dialog = dialogBuilder.create();
//        edtPrice.setText(String.valueOf(inventory.getSellingPrice()));

        btnAddToSale.setOnClickListener(new OnclickEvent(addToCartListener, edtQuantity, product, dialog));

        return dialog;
    }

    public static CFAlertDialog loginErrorAlert(Context context, String title, String message, CFAlertDialog.CFAlertStyle cfAlertStyle) {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                .setDialogStyle(cfAlertStyle)
                .setAutoDismissAfter(1500)
                .setTextGravity(Gravity.START)
                .setTitle(title).setIcon(R.drawable.ic_error_red_800_24dp).setMessage(message);

        return builder.create();
    }

    public static CFAlertDialog successAlert(Context context, String message, CFAlertDialog.CFAlertStyle cfAlertStyle) {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                .setDialogStyle(cfAlertStyle)
                .setTextColor(ContextCompat.getColor(context, android.R.color.holo_green_dark))
                .setOuterMargin(0)
                .setAutoDismissAfter(1500)
                .setTextGravity(Gravity.START)
                .setMessage(message)
                .setCornerRadius(0.0f)
                .setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));

        return builder.create();
    }

    public static CFAlertDialog formSubmitError(Context context, String title, String[] errorsArray, CFAlertDialog.CFAlertStyle cfAlertStyle) {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                .setDialogStyle(cfAlertStyle)
                .setAutoDismissAfter(4000)
                .setTextGravity(Gravity.START)
                .setTitle(title).setIcon(R.drawable.ic_error_red_800_24dp).setItems(errorsArray, null);

        return builder.create();
    }

    private static class OnclickEvent implements View.OnClickListener {
        private OnAddToCartListener listener;
        private EditText edtQuantity;
        private Product product;
        private CFAlertDialog dialog;

        private OnclickEvent(OnAddToCartListener listener, EditText quantity, Product product, CFAlertDialog dialog) {
            this.listener = listener;
            this.edtQuantity = quantity;
            this.product = product;
            this.dialog = dialog;
        }

        @Override
        public void onClick(View v) {
            String quantity = edtQuantity.getText().toString().trim();
            if (quantity.length() < 1) {
                Snackbar.make(v, "Please provide quantity to order", Snackbar.LENGTH_SHORT).setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        dialog.dismiss();
                    }
                }).show();
                return;
            }
            OrderItem orderItem = new OrderItem();
//            orderItem.setInventory(inventory);
//            saleItem.setSale(null);
//            saleItem.setItemAmount(Integer.parseInt(quantity) * inventory.getSellingPrice());
//            saleItem.setItemQuantity(Integer.parseInt(quantity));
//            saleItem.setSellingPrice(inventory.getSellingPrice());
//            listener.onItemAddedToSale(v, saleItem);
            listener.onAddedToCart(product);
            dialog.dismiss();
        }
    }

}
