package com.zeffah.shoppa.listadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeffah.shoppa.R;
import com.zeffah.shoppa.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder> implements Filterable {

    private List<Product> productList;
    private List<Product> listSuggestions;
    private final OnAddItemToCart onAddItemToCart;

    public ProductRecyclerViewAdapter(List<Product> productList, OnAddItemToCart onAddItemToCart) {
        this.productList = productList;
        this.onAddItemToCart = onAddItemToCart;
        this.listSuggestions = productList;
    }

    public void updateList(List<Product> newList){
        listSuggestions = newList;
        notifyDataSetChanged();
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final Product product = listSuggestions.get(position);
        holder.txtProductName.setText(product.getProductName());
        holder.txtProductDesc.setText(product.getProductDetails());
        holder.txtProductPrice.setText(String.valueOf("Kes." + product.getProductPrice()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onAddItemToCart) {
                    onAddItemToCart.addToCart(product);
                }
            }
        });

        holder.imgAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onAddItemToCart) {
                    onAddItemToCart.addToCart(product);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listSuggestions.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Product> filteredList = new ArrayList<>();
                if (constraint.toString().isEmpty()) {
                    filteredList = productList;
                } else {
                    for (Product product : productList) {
                        if (product.getProductName().toLowerCase().contains(constraint.toString())) {
                            filteredList.add(product);
                        }
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override @SuppressWarnings("unchecked")
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listSuggestions = (ArrayList<Product>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnAddItemToCart {
        void addToCart(Product product);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView txtProductName;
        final TextView txtProductDesc;
        final TextView txtProductPrice;
        final ImageView imgAddToCart;

        ViewHolder(View view) {
            super(view);
            mView = view;
            txtProductName = view.findViewById(R.id.txt_product_name);
            txtProductDesc = view.findViewById(R.id.txt_product_description);
            txtProductPrice = view.findViewById(R.id.txt_product_price);
            imgAddToCart = view.findViewById(R.id.img_add_shopping_cart);
        }

        @Override
        public String toString() {
            return txtProductName.getText().toString();
        }
    }
}
