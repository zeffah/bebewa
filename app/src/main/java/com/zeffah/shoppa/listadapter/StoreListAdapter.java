package com.zeffah.shoppa.listadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeffah.shoppa.R;

import java.util.List;

public class StoreListAdapter extends ArrayAdapter<String> {
    private Context context;
    private int resource;
    private List<String> storeList;

    public StoreListAdapter(@NonNull Context context, int resource, int txtViewId, @NonNull List<String> storeList) {
        super(context, resource, txtViewId, storeList);
        this.context = context;
        this.resource = resource;
        this.storeList = storeList;
    }

    @Override
    public int getCount() {
        return storeList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return storeList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public static class StoreListViewHolder {
        private View convertView;
        ImageView imgStoreIcon;
        TextView txtStoreName;

        StoreListViewHolder(View itemView) {
            this.convertView = itemView;
            this.imgStoreIcon = this.convertView.findViewById(R.id.img_shop_logo);
            this.txtStoreName = this.convertView.findViewById(R.id.txt_store_name);
        }
    }

    private View getCustomView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_view, parent, false);
            StoreListViewHolder viewHolder = new StoreListViewHolder(view);
            view.setTag(viewHolder);
        }

        StoreListViewHolder holder = (StoreListViewHolder) view.getTag();
        final String store = storeList.get(position);
        holder.txtStoreName.setText(store);
//        holder.imgStoreIcon.setImageResource(R.mipmap.ic_launcher_round);
        return view;
    }
}
