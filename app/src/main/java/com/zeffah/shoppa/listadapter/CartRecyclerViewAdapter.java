package com.zeffah.shoppa.listadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zeffah.shoppa.R;
import com.zeffah.shoppa.main.CartFragment;
import com.zeffah.shoppa.model.OrderItem;

import java.util.List;
import java.util.Locale;

public class CartRecyclerViewAdapter extends RecyclerView.Adapter<CartRecyclerViewAdapter.ViewHolder> {
    private final List<OrderItem> orderItems;
    private final OnQuantityChange onQuantityChange;

    public CartRecyclerViewAdapter(List<OrderItem> orderItems, OnQuantityChange onQuantityChange) {
        this.orderItems = orderItems;
        this.onQuantityChange = onQuantityChange;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OrderItem orderItem = orderItems.get(position);
        orderItem.product.load();
        holder.txtProductName.setText(String.format(Locale.getDefault(), "%s - %s", orderItem.getItemName(), orderItem.getItemType()));
        holder.txtItemQty.setText(String.valueOf(orderItem.getItemQuantity()));
        holder.txtProductPrice.setText(String.valueOf("Ksh." + orderItem.product.getProductPrice()));

        holder.imgIncreaseCart.setOnClickListener(new OnViewClick(orderItem, holder, holder.getAdapterPosition()));
        holder.imgReduceCart.setOnClickListener(new OnViewClick(orderItem, holder, holder.getAdapterPosition()));
        holder.imgIncreaseCart.setOnClickListener(new OnViewClick(orderItem, holder, holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }


    public interface OnQuantityChange {
        void onChange(String action, OrderItem orderItem, int newQty);
    }

    private class OnViewClick implements View.OnClickListener {
        private ViewHolder viewHolder;
        private OrderItem orderItem;
        private int position;

        private OnViewClick(OrderItem orderItem, ViewHolder viewHolder, int position) {
            this.viewHolder = viewHolder;
            this.orderItem = orderItem;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            if (v ==viewHolder.imgRemoveItem){
                if (orderItem.delete()) {
                    orderItems.remove(position);
                    notifyDataSetChanged();
                    onQuantityChange.onChange(CartFragment.ACTION_REDUCE_CART, orderItem, orderItem.getItemQuantity());
                }
                return;
            }
            if (v == viewHolder.imgReduceCart){
                String _currentQty = viewHolder.txtItemQty.getText().toString();
                int qty = 0;
                if (_currentQty.length() > 0) {
                    qty = Integer.parseInt(_currentQty);
                    if (qty > 1) {
                        qty -= 1;
                        orderItem.setItemQuantity(qty);
                        if (orderItem.save()) {
                            onQuantityChange.onChange(CartFragment.ACTION_REDUCE_CART, orderItem, qty);
                        }
                    }
                }
                viewHolder.txtItemQty.setText(String.valueOf(qty));
                return;
            }
            if (v == viewHolder.imgIncreaseCart){
                String _currentQty = viewHolder.txtItemQty.getText().toString();
                int qty = 1;
                if (_currentQty.length() > 0) {
                    qty = Integer.parseInt(_currentQty);
                    qty += 1;
                }
                orderItem.setItemQuantity(qty);
                if (orderItem.save()) {
                    onQuantityChange.onChange(CartFragment.ACTION_INCREASE_CART, orderItem, qty);
                }
                viewHolder.txtItemQty.setText(String.valueOf(qty));
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView txtProductName;
        final TextView txtItemQty;
        final TextView txtProductPrice;
        final ImageView imgIncreaseCart, imgReduceCart, imgRemoveItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            txtProductName = view.findViewById(R.id.txt_product_name);
            txtItemQty = view.findViewById(R.id.txt_order_quantity);
            imgIncreaseCart = view.findViewById(R.id.img_increase_quantity);
            imgReduceCart = view.findViewById(R.id.img_reduce_quantity);
            imgRemoveItem = view.findViewById(R.id.id_remove_cart_item);
            txtProductPrice = view.findViewById(R.id.txt_product_price);
        }
    }
}

