package com.zeffah.shoppa.listadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.zeffah.shoppa.R;
import com.zeffah.shoppa.model.BestStore;

import java.util.List;

public class BestStoreAdapter extends ArrayAdapter<BestStore>{
    private List<BestStore> bestStoreList;
    private Context context;
    private int selectedPosition = 0;
    public BestStoreAdapter(@NonNull Context context, int resource, @NonNull List<BestStore> bestStoreList) {
        super(context, resource, bestStoreList);
        this.context = context;
        this.bestStoreList = bestStoreList;
    }

    @Override
    public int getCount() {
        return bestStoreList.size();
    }

    @Nullable
    @Override
    public BestStore getItem(int position) {
        return bestStoreList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.best_store_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        ViewHolder mHolder = (ViewHolder) convertView.getTag();
        BestStore bestStore = bestStoreList.get(position);
        mHolder.txtShoppingAmount.setText(String.valueOf(bestStore.getShoppingAmount()));
        mHolder.txtStoreName.setText(bestStore.getStoreName());
        mHolder.rdBtnSelectedStore.setChecked(position == selectedPosition);
        mHolder.rdBtnSelectedStore.setTag(position);

        mHolder.rdBtnSelectedStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
            }
        });
        return convertView;
    }

    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        notifyDataSetChanged();
    }

    public BestStore getSelectedItem() {
        if (selectedPosition != -1) {
            return bestStoreList.get(selectedPosition);
        }
        return null;
    }

    private class ViewHolder {
        private View view;
        private TextView txtStoreName, txtShoppingAmount;
        private RadioButton rdBtnSelectedStore;
        ImageView imgStoreLogo;
        ViewHolder(View v){
            this.view = v;
            this.txtStoreName = view.findViewById(R.id.txt_store_name);
            this.txtShoppingAmount = view.findViewById(R.id.txt_shopping_amount);
            this.rdBtnSelectedStore = view.findViewById(R.id.rd_selected_store);
            this.imgStoreLogo = view.findViewById(R.id.img_shop_logo);

        }
    }
}
