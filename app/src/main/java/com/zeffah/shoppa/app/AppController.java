package com.zeffah.shoppa.app;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowManager;

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(getApplicationContext());
        Stetho.initializeWithDefaults(this);
    }
}
