package com.zeffah.shoppa.app;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = APPDatabase.NAME, version = APPDatabase.VERSION)
public class APPDatabase {
    static final String NAME = "db_shoppa_app";
    static final int VERSION = 1;
}
