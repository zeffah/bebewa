package com.zeffah.shoppa.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.zeffah.shoppa.app.APPDatabase;

@Table(name = "product", database = APPDatabase.class)
public class Product extends BaseModel{
    @Column(name = "id")
    @PrimaryKey(autoincrement = true)
    private int productId;
    @Column(name = "bar_code")
    private String barCode;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "product_detail")
    private String productDetails;
    @Column(name = "product_price")
    private double productPrice;
    @ForeignKey(stubbedRelationship = true)
    public Store store;

    public Product() {}

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Override
    public String toString() {
        return productName;
    }
}
