package com.zeffah.shoppa.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.zeffah.shoppa.app.APPDatabase;

@Table(name = "order_item", database = APPDatabase.class)
public class OrderItem extends BaseModel{
    @Column(name = "id")
    @PrimaryKey(autoincrement = true)
    private int orderItemId;
    @Column(name = "quantity")
    private int itemQuantity;
    @Column(name = "item_name")
    private String itemName;
    @Column(name = "item_type")
    private String itemType;
    @Column(name = "bar_code")
    private String barCode;
    @ForeignKey(stubbedRelationship = true)
    public Product product;
    @ForeignKey(stubbedRelationship = true)
    private Order order;

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
