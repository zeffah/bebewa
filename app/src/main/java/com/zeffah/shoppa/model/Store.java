package com.zeffah.shoppa.model;

import android.graphics.drawable.Drawable;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.zeffah.shoppa.app.APPDatabase;

import java.util.List;

@Table(name = "store", database = APPDatabase.class)
public class Store extends BaseModel {
    @Column(name = "id")
    @PrimaryKey(autoincrement = true)
    private int storeId;
    @Column(name = "store_name")
    private String storeName;
    @Column(name = "store_slogan")
    private String storeSlogan;
    @Column(name = "logo")
    private int storeLogo;

    public List<StoreBranch> storeBranches;

    @OneToMany(methods = OneToMany.Method.ALL, variableName = "storeBranches")
    public List<StoreBranch> storeBranchesList(){
        if (storeBranches == null){
            storeBranches = SQLite.select().from(StoreBranch.class).where(StoreBranch_Table.store_id.eq(storeId)).queryList();
        }
        return storeBranches;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreSlogan() {
        return storeSlogan;
    }

    public void setStoreSlogan(String storeSlogan) {
        this.storeSlogan = storeSlogan;
    }

    public int getStoreLogo() {
        return storeLogo;
    }

    public void setStoreLogo(int storeLogo) {
        this.storeLogo = storeLogo;
    }

    @Override
    public String toString() {
        return getStoreName();
    }
}
