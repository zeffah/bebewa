package com.zeffah.shoppa.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.zeffah.shoppa.app.APPDatabase;

import java.util.List;

@Table(name = "order", database = APPDatabase.class)
public class Order extends BaseModel{
    @Column(name = "id")
    @PrimaryKey(autoincrement = true)
    private int orderId;
    @Column(name = "order_number")
    private String orderNumber;
    @Column(name = "order_date")
    private long orderDate;
    public List<OrderItem> orderItems;

    @OneToMany(methods = OneToMany.Method.ALL, variableName = "orderItems")
    public List<OrderItem> orderItemList(){
        if (orderItems == null){
            orderItems = SQLite.select().from(OrderItem.class).where(OrderItem_Table.order_id.eq(orderId)).queryList();
        }
        return orderItems;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public long getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(long orderDate) {
        this.orderDate = orderDate;
    }
}
