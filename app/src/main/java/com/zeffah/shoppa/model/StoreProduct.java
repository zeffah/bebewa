package com.zeffah.shoppa.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.zeffah.shoppa.app.APPDatabase;

@Table(name = "store_product", database = APPDatabase.class)
public class StoreProduct extends BaseModel {
    @Column(name = "id")
    @PrimaryKey(autoincrement = true)
    private int storeProductId;
    @Column(name = "price")
    private double productPrice;
    @ForeignKey(stubbedRelationship = true)
    public Product product;
    @ForeignKey(stubbedRelationship = true)
    public Store store;

    public int getStoreProductId() {
        return storeProductId;
    }

    public void setStoreProductId(int storeProductId) {
        this.storeProductId = storeProductId;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
