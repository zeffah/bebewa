package com.zeffah.shoppa.model;

import android.annotation.TargetApi;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.zeffah.shoppa.app.APPDatabase;

@Table(name = "store_branch", database = APPDatabase.class)
public class StoreBranch extends BaseModel {
    @Column(name = "id")
    @PrimaryKey(autoincrement = true)
    private int branchId;
    @Column(name = "location")
    private String location;
    @ForeignKey(stubbedRelationship = true)
    private Store store;

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
