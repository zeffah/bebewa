package com.zeffah.shoppa.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ProductList {

    public static final List<Product> PRODUCT_ARRAY_LIST = new ArrayList<>();

//    public static final Map<String, Product> PRODUCT_ARRAY_MAP = new HashMap<>();

    private static final int COUNT = 25;

    static {
        for (int i = 1; i <= COUNT; i++) {
            addItem(createProductItem(i));
        }
    }

    private static void addItem(Product item) {
        PRODUCT_ARRAY_LIST.add(item);
//        PRODUCT_ARRAY_MAP.put(item.id, item);
    }

    private static Product createProductItem(int position) {
        Product product = new Product();
        product.setProductId(position);
        product.setProductName(String.valueOf("Product "+position));
        product.setProductDetails(makeDetails(position));
        product.setProductPrice(500.00);
        return product;
    }

    private static String makeDetails(int position) {
        return String.format(Locale.getDefault(), "Details about Product %d", position);
    }


}
