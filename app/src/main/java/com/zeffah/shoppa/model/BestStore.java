package com.zeffah.shoppa.model;

public class BestStore {
    private String storeName;
    private double shoppingAmount;

    public BestStore(String storeName, double shoppingAmount) {
        this.storeName = storeName;
        this.shoppingAmount = shoppingAmount;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public double getShoppingAmount() {
        return shoppingAmount;
    }

    public void setShoppingAmount(double shoppingAmount) {
        this.shoppingAmount = shoppingAmount;
    }
}
