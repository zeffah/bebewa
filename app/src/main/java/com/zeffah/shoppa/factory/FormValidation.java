package com.zeffah.shoppa.factory;

import android.support.v4.content.ContextCompat;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.raizlabs.android.dbflow.StringUtils;
import com.zeffah.shoppa.R;

import java.util.ArrayList;
import java.util.List;

public class FormValidation {

    public static List<String> registrationFormValid(View view) {
        List<String> errorsArray = new ArrayList<>();
        EditText edtFirstName = view.findViewById(R.id.edt_first_name);
        EditText edtLastName = view.findViewById(R.id.edt_last_name);
        EditText edtPhoneNumber = view.findViewById(R.id.edt_phone_number);
        EditText edtEmailAddress = view.findViewById(R.id.edt_email_address);
        EditText edtUserName = view.findViewById(R.id.edt_username);
        EditText edtPassword = view.findViewById(R.id.edt_password);
        EditText edtConfirmPassword = view.findViewById(R.id.edt_confirm_password);

        String phoneNumber = edtPhoneNumber.getText().toString().trim();
        String emailAddress = edtEmailAddress.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();
        String confirmPass = edtConfirmPassword.getText().toString().trim();

        if (StringUtils.isNullOrEmpty(edtFirstName.getText().toString().trim())) {
            errorsArray.add("First name not provided");
            edtFirstName.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        } else {
            edtFirstName.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.edit_text_round_background));
        }
        if (StringUtils.isNullOrEmpty(edtLastName.getText().toString().trim())) {
            errorsArray.add("Last name not provided");
            edtLastName.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        } else {
            edtLastName.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.edit_text_round_background));
        }
        if (StringUtils.isNullOrEmpty(phoneNumber)) {
            errorsArray.add("Phone number must be provided");
            edtPhoneNumber.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        } else if(phoneNumber.length() < 10 || phoneNumber.length() > 10){
            errorsArray.add("Invalid Phone number. Must be 10 characters long");
            edtPhoneNumber.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        }else if(!phoneNumber.startsWith(String.valueOf("07"))){
            errorsArray.add("Phone number should be in the form of 07xxxxxxxx");
            edtPhoneNumber.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        }else {
            edtPhoneNumber.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.edit_text_round_background));
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()) {
            errorsArray.add("Invalid email address");
            edtEmailAddress.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        } else {
            edtEmailAddress.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.edit_text_round_background));
        }
        if (StringUtils.isNullOrEmpty(edtUserName.getText().toString().trim())) {
            errorsArray.add("Username is empty");
            edtUserName.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        } else {
            edtUserName.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.edit_text_round_background));
        }
        if (StringUtils.isNullOrEmpty(password)) {
            errorsArray.add("Password is blank");
            edtPassword.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        } else {
            edtPassword.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.edit_text_round_background));
        }
        if (StringUtils.isNullOrEmpty(confirmPass)) {
            errorsArray.add("Confirm password is empty");
            edtConfirmPassword.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        } else {
            edtConfirmPassword.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.edit_text_round_background));
        }
        if (!password.equals(confirmPass)) {
            errorsArray.add("Password do not match");
            edtConfirmPassword.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
            edtPassword.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.error_edit_text_background));
        }
        return errorsArray;
    }
}
