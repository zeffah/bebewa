package com.zeffah.shoppa.factory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;
import com.zeffah.shoppa.R;
import com.zeffah.shoppa.callback.OnCartBottomSheetDismiss;
import com.zeffah.shoppa.listadapter.BestStoreAdapter;
import com.zeffah.shoppa.model.BestStore;
import com.zeffah.shoppa.model.Product;
import com.zeffah.shoppa.model.Product_Table;
import com.zeffah.shoppa.model.Store;
import com.zeffah.shoppa.model.Store_Table;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class GlobalMethods {
    public static void setActionBarTitle(ActionBar actionBar, String title, boolean homeAsUpEnabled) {
        if (actionBar != null) {
            if (!actionBar.isShowing()) actionBar.show();
            actionBar.setDisplayHomeAsUpEnabled(homeAsUpEnabled);
            actionBar.setHomeButtonEnabled(homeAsUpEnabled);
            actionBar.setTitle(title);
        }
    }

    public static BottomDialog cartDialog(Context context, final OnCartBottomSheetDismiss onCartBottomSheetDismiss) {
        View customView = LayoutInflater.from(context).inflate(R.layout.best_shops_listview, null, false);
        ListView listView = customView.findViewById(R.id.list_best_stores);
        List<BestStore> bestStores = new ArrayList<>();
        BestStore bestStore0 = new BestStore("Naivas", 8000);
        BestStore bestStore1 = new BestStore("Nakumatt", 8100);
        BestStore bestStore2 = new BestStore("Tuskys", 7900);
        bestStores.add(bestStore0);
        bestStores.add(bestStore1);
        bestStores.add(bestStore2);
        final BestStoreAdapter adapter = new BestStoreAdapter(context, 0, bestStores);
        listView.setAdapter(adapter);
        return new BottomDialog.Builder(context)
                .setTitle("My Want to Change Decision?")
                .setPositiveText("Checkout")
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setContent("Here are best offers to choose from.")
                .setCustomView(customView)
                .onPositive(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(@NonNull BottomDialog dialog) {
                        dialog.dismiss();
                        onCartBottomSheetDismiss.onDismiss(adapter.getSelectedItem());
                    }
                })
                .build();
    }

    public static String generateRandomString(int len) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static void generateProductList() {
        Store store1 = SQLite.select().from(Store.class).where(Store_Table.id.eq(2)).querySingle();
        Store store2 = SQLite.select().from(Store.class).where(Store_Table.id.eq(1)).querySingle();
        Store store3 = SQLite.select().from(Store.class).where(Store_Table.id.eq(3)).querySingle();
        Store store4 = SQLite.select().from(Store.class).where(Store_Table.id.eq(4)).querySingle();
        List<Product> productList = SQLite.select().from(Product.class).queryList();
        if (productList.isEmpty()) {

            //store id 2 - Naivas
            Product product = new Product();
            product.setProductName("Super White B/Toast");
            product.setProductPrice(95);
            product.setProductDetails("800G");
            product.setBarCode(String.valueOf("9897468347346848"));
            product.store = store1;
            product.store.save();
            product.save();

            Product product1 = new Product();
            product1.setProductName("Prestige margarine");
            product1.setProductPrice(120);
            product1.setProductDetails("250g");
            product1.setBarCode(String.valueOf("7327392837292332"));
            product1.store = store1;
            product1.store.save();
            product1.save();

            Product product2 = new Product();
            product2.setProductName("Excel Prem Drinking water");
            product2.setProductPrice(45);
            product2.setProductDetails("1L");
            product2.setBarCode(String.valueOf("8823289823729382"));
            product2.store = store1;
            product2.store.save();
            product2.save();

            Product product3 = new Product();
            product3.setProductName("Naivas Fresh Cream Milk");
            product3.setProductPrice(60);
            product3.setProductDetails("1Litr");
            product3.setBarCode(String.valueOf("7327332924884882"));
            product3.store = store1;
            product3.store.save();
            product3.save();

            //store id 2 - carrefour

            Product product4 = new Product();
            product4.setProductName("Super White Butter Toast");
            product4.setProductPrice(98);
            product4.setProductDetails("800G");
            product4.setBarCode(String.valueOf("9897468347346848"));
            product4.store = store2;
            product4.store.save();
            product4.save();

            Product product5 = new Product();
            product5.setProductName("Prestige margarine Bread Spread");
            product5.setProductPrice(125);
            product5.setProductDetails("250g");
            product5.setBarCode(String.valueOf("7327392837292332"));
            product5.store = store2;
            product5.store.save();
            product5.save();

            Product product6 = new Product();
            product6.setProductName("Excel Drinking water");
            product6.setProductPrice(41);
            product6.setProductDetails("1Ltr");
            product6.setBarCode(String.valueOf("8823289823729382"));
            product6.store = store2;
            product6.store.save();
            product6.save();

            Product product7 = new Product();
            product7.setProductName("Fresh Cream Milk");
            product7.setProductPrice(52);
            product7.setProductDetails("1L");
            product7.setBarCode(String.valueOf("7327332924884882"));
            product7.store = store2;
            product7.store.save();
            product7.save();

            //store id 3 - Nakumatt

            Product product8 = new Product();
            product8.setProductName(" Butter Toast Super");
            product8.setProductPrice(99);
            product8.setProductDetails("800G");
            product8.setBarCode(String.valueOf("9897468347346848"));
            product8.store = store3;
            product8.store.save();
            product8.save();

            Product product9 = new Product();
            product9.setProductName("Margarine Bread Spread");
            product9.setProductPrice(118);
            product9.setProductDetails("250g");
            product9.setBarCode(String.valueOf("7327392837292332"));
            product9.store = store3;
            product9.store.save();
            product9.save();

            Product product10 = new Product();
            product10.setProductName("Excel Fresh water");
            product10.setProductPrice(50);
            product10.setProductDetails("1Lt");
            product10.setBarCode(String.valueOf("8823289823729382"));
            product10.store = store3;
            product10.store.save();
            product10.save();

            Product product11 = new Product();
            product11.setProductName("Fresh CR Milk");
            product11.setProductPrice(58);
            product11.setProductDetails("1Lt");
            product11.setBarCode(String.valueOf("7327332924884882"));
            product11.store = store3;
            product11.store.save();
            product11.save();

//            store 4 - Tuskys
            Product product12 = new Product();
            product12.setProductName(" Butter Toast Super Loaf");
            product12.setProductPrice(95);
            product12.setProductDetails("800GM");
            product12.setBarCode(String.valueOf("9897468347346848"));
            product12.store = store4;
            product12.store.save();
            product12.save();

            Product product13 = new Product();
            product13.setProductName("Margarine Blue");
            product13.setProductPrice(123);
            product13.setProductDetails("250gm");
            product13.setBarCode(String.valueOf("7327392837292332"));
            product13.store = store4;
            product13.store.save();
            product13.save();

            Product product14 = new Product();
            product14.setProductName("Excel Clean water");
            product14.setProductPrice(52);
            product14.setProductDetails("1Lt");
            product14.setBarCode(String.valueOf("8823289823729382"));
            product14.store = store4;
            product14.store.save();
            product14.save();

            Product product15 = new Product();
            product15.setProductName("Creamier Fresh Milk");
            product15.setProductPrice(40);
            product15.setProductDetails("1Lt");
            product15.setBarCode(String.valueOf("7327332924884882"));
            product15.store = store4;
            product15.store.save();
            product15.save();

            Log.d("ProductsCreate", "products saved");
        }
        Log.d("ProductsCreate", "products exist");
    }

    public static void generateStores() {
        List<Store> stores = SQLite.select().from(Store.class).queryList();
        if (stores.isEmpty()) {
            Store store = new Store();
            store.setStoreName("Carrefour");
            store.save();
            Store store1 = new Store();
            store1.setStoreName("Naivas");
            store1.save();
            Store store2 = new Store();
            store2.setStoreName("Nakumatt");
            store2.save();
            Store store3 = new Store();
            store3.setStoreName("Tuskys");
            store3.save();
        }
    }

    public static void fetchProductList(final OnFetchListener onFetchListener) {
        SQLite.select().from(Product.class).async().queryListResultCallback(new QueryTransaction.QueryResultListCallback<Product>() {
            @Override
            public void onListQueryResult(QueryTransaction transaction, @NonNull List<Product> tResult) {
                if (!tResult.isEmpty()) {
                    onFetchListener.onFetchSuccess(tResult);
                }
            }
        }).error(new Transaction.Error() {
            @Override
            public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {

            }
        }).execute();
    }

    public static void fetchStoreList(final OnFetchListener onFetchListener) {
        SQLite.select().from(Store.class).async().queryListResultCallback(new QueryTransaction.QueryResultListCallback<Store>() {
            @Override
            public void onListQueryResult(QueryTransaction transaction, @NonNull List<Store> tResult) {
                if (!tResult.isEmpty()) {
                    onFetchListener.onFetchSuccess(tResult);
                }
            }
        }).error(new Transaction.Error() {
            @Override
            public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {

            }
        }).execute();
    }

    public static List<Product> getStoreProducts(Store store) {
        return SQLite.select().from(Product.class).where(Product_Table.store_id.eq(store.getStoreId())).queryList();
    }

    public interface OnFetchListener {
        void onFetchSuccess(List<?> results);
    }
}
