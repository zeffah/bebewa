package com.zeffah.shoppa.factory;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.zeffah.shoppa.model.OrderItem;
import com.zeffah.shoppa.model.Product;
import com.zeffah.shoppa.model.Product_Table;
import com.zeffah.shoppa.model.Store;
import com.zeffah.shoppa.modelService.OrderService;

import java.util.List;

public class AppViewModel extends ViewModel {

    private MutableLiveData<List<OrderItem>> orderItems;
    private MutableLiveData<List<Store>> stores;
    private MutableLiveData<List<Product>> products;
    public LiveData<List<OrderItem>> getOrdeItems(){
        if (orderItems != null){
            orderItems = new MutableLiveData<>();
            loadItems();
        }
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItemsList){
        orderItems.setValue(orderItemsList);
    }

    public LiveData<List<Store>> getStores(){
        if (stores != null){
            return stores;
        }
        return null;
    }

    public void setStores(List<Store> storeList){
        stores = new MutableLiveData<>();
        stores.setValue(storeList);
    }

    public LiveData<List<Product>> getStoreProducts(){
        if (products != null){
            return products;
        }
        return null;
    }

    public void setStoreProducts(Store store){
        products = new MutableLiveData<>();
        List<Product> productList = SQLite.select().from(Product.class).where(Product_Table.store_id.eq(store.getStoreId())).queryList();
        products.setValue(productList);
    }

    private void loadItems(){
        orderItems.setValue(OrderService.orderItems());
    }

}
