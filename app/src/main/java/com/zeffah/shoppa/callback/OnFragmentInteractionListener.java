package com.zeffah.shoppa.callback;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(String uri);
}
