package com.zeffah.shoppa.callback;

import com.zeffah.shoppa.model.Product;

public interface OnAddToCartListener {
    void onAddedToCart(Product product);
}
