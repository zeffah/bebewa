package com.zeffah.shoppa.callback;

import com.zeffah.shoppa.model.BestStore;

public interface OnCartBottomSheetDismiss {
    void onDismiss(BestStore best);
}
